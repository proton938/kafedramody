/**
 * Class for cookies control
 * @class Cookies
 * @singletone
 *
 * @version 0.1
 *
 */
window.Cookies = (function() {
    var EXPIRES_REGEXP = /\+(\d+)(h|d|m|y)/;

    return {
        /**
         * Устанавливает значение cookie
         * @param {String} name
         * @param {String} value
         * @param {String} expires возможно использовать инкрементные значения (+1d, +2m ...)
         * @param {String} path
         * @param {String} domain
         * @param {Boolean} secure
         */
        set : function(name, value, expires, path, domain, secure) {
            if (expires) {
                expires = expires.toString().match(EXPIRES_REGEXP);
            }
            if (expires) {
                var date = new Date();
                var c = expires[1];
                var s = expires[2];
                switch (s) {
                    case 'y':
                        date.setFullYear(date.getFullYear() + c);
                        break;
                    case 'm':
                        date.setMonth(date.getMonth() + c);
                        break;
                    case 'd':
                        date.setDate(date.getDate() + c);
                        break;
                    case 'h':
                        date.setHours(date.getHours() + c);
                        break;
                }
                expires = date.toUTCString();
            }
            document.cookie = name + "=" + escape(value) +
                ((expires) ? "; expires=" + expires : "") +
                ((path) ? "; path=" + path : "") +
                ((domain) ? "; domain=" + domain : "") +
                ((secure) ? "; secure" : "");
        },
        /**
         * Получает значение cookie
         * @param {String} name
         */
        get : function(name) {
            var cookie = " " + document.cookie;
            var search = " " + name + "=";
            var setStr = null;
            var offset = 0;
            var end = 0;
            if (cookie.length > 0) {
                offset = cookie.indexOf(search);
                if (offset != -1) {
                    offset += search.length;
                    end = cookie.indexOf(";", offset);
                    if (end == -1)
                        end = cookie.length;
                    setStr = unescape(cookie.substring(offset, end));
                }
            }
            return(setStr);
        }
    };
})();