$(document).on('ready', function() {
        // Получаем data-dev в теге body - включён ли у нас режим тестирования  
    var DataDevBody = $(".body-wrap").data("dev");

        Proj.HomeCourseMore();
        Proj.HomeMenuXs();
        Proj.Scroll();
        Proj.FancyBox();
        Proj.Filter();
        Proj.ShowType();
        Proj.Modal();
        Proj.Form('.form');
        Proj.ProdForm();
        Proj.FixedBanner();

        if ($('body').is('.is--mobile')) {
            Proj.Mobile();
        }
        $('.js-height').each(function(e){
            console.log($(this).height(),$(this).parent().height());
            if ($(this).parent().height() > $(this).height()) {
                $(this).parent().addClass('m--noborder');
            }
        });
        var mheight = ($('.left').height() > $('.right').height()) ? $('.left').height() : $('.right').height();
        $('.place').css('min-height', mheight+115);
        $('.slick').slick({
            infinite: true,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true
        });
        $('.js-gallery').slick({
            infinite: true,
            slidesToShow: 2,
        });
   
       
    
});