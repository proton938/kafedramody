"use strict";
/**
 * Main Class and namespace
 * @class Proj
 * @singletone
 */
var Proj = (function(proj) {
	proj.interfaceName = '';

	proj.init = function() {
		proj.interfaceName = $('body').data("interfaceName");
	};

	return proj;

})(Proj || {});

/** @type {String} url для ajax запроса */
var AJAX_URI = '/json/';

/** @type {String} id сессии из командной строки */
var SESS_ID = '';
if (window.location.toString().match(/SESS_ID=(\d+)/))
	SESS_ID = RegExp.$1;
//------------------------------------------------------------------------------
/* @param {Function} handler Обработчик */
Proj.getJSON = function(data, handler) {
	if (SESS_ID)
		data['SESS_ID'] = SESS_ID;
	$.post(AJAX_URI, data, handler, 'json');
}

/**
  *  Обработчик fancybox
  */
Proj.FancyBox = function(){
	$(".fancybox").fancybox({
	    wrapCSS: 'fancybox-wrap-inner',
	    prevEffect: 'fade',
	    nextEffect: 'fade',
	});
}

/**
    *  Тип отображения расписания
    */
Proj.ShowType = function(){
    var $type = $('.courses').find('.js-show-types');
	$type.on('click', function(){
	    $type.removeClass('m--active');
        $(this).addClass('m--active');
        var type = $(this).data('type');
        _setShowType(type);
	});
    function _setShowType(type){
    console.log(type);
    Proj.getJSON( {
            '_do': 'set_show_type',
            'type': type,
        }, _responseSetShowType);
    }
    function _responseSetShowType(data){
        if (data.status == 'ok') {
            window.location.reload(true);
        }
    }
    if ($(window).width() < 767) {
        console.log($('.courses').find('.js-show-types').filter('[data-type=list]'));
        $('.courses').find('.js-show-types').not('.m--active').filter('[data-type=list]').trigger('click');
    }
}

/**
  *  Фильтр расписания
  */
Proj.Filter = function(){
    Proj.ScrollFilter();
    $('.js-filter-toggle').on('click', function(){
        $(this).siblings('.filter__block').animate({height:'toggle'},350);
        $(this).find('span').toggleClass('m--open m--close');
    });
    var $address = $('.filter').find('.js-filter-address');
    var $time = $('.filter').find('.js-filter-time');
    var $letter = $('.filter').find('.js-filter-letter');
	$address.on('click', function(){
      var id = $(this).data('id');
	    if ($(this).hasClass('m--active')) {
            $address.filter('[data-id="'+id+'"]').removeClass('m--active');
	    } else {
            $address.filter('[data-id="'+id+'"]').addClass('m--active');
	    }
        _setFilter();
	});
	$time.on('click', function(){
      var id = $(this).data('id');
	    if ($(this).hasClass('m--active')) {
            $time.filter('[data-id="'+id+'"]').removeClass('m--active');
            $(this).removeClass('m--active');
	    } else {
            $time.filter('[data-id="'+id+'"]').addClass('m--active');
	    }
        _setFilter();
	});
	$letter.on('click', function(){
      var id = $(this).data('id');
	    if ($(this).hasClass('m--active')) {
            $letter.filter('[data-id="'+id+'"]').removeClass('m--active');
	    } else {
            $letter.filter('[data-id="'+id+'"]').addClass('m--active');
	    }
        _setFilter();
	});
    function _setFilter(){
        var filter_address = [];
        var filter_times = [];
        var filter_letters = [];
        $address.each(function(e){
            if ($address.eq(e).hasClass('m--active')) filter_address.push($address.eq(e).data('id'));
        });
        $letter.each(function(e){
            if ($letter.eq(e).hasClass('m--active')) filter_letters.push($letter.eq(e).data('child_id'));
        });
        $time.each(function(e){
            if ($time.eq(e).hasClass('m--active')) filter_times.push($time.eq(e).data('id'));
        });
        console.log(filter_address,filter_letters,filter_times);
        Proj.getJSON( {
                '_do': 'set_filter',
                'addresses': filter_address.join(','),
                'letters': filter_letters.join(','),
                'times': filter_times.join(',')
            }, _responseSetFilter);
    }
    function _responseSetFilter(data){
        if (data.status == 'ok') {
            window.location.reload(true);
        }
    }
}
//------------------------------------------------------------------------------
Proj.ScrollFilter = function() {
    if ($('div').is('.js-filter')) {
        var $place = $(".place");
        var $block = $(".js-filter .filter");
        initScroll();
    }
    function initScroll() {

        var offsetTop = $block.offset().top;
        var pHeight = $place.outerHeight();
        var bHeight = $block.innerHeight();
        //console.log(pHeight,bHeight);
        function scrollHandler(e) {
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            //console.log(scrollTop,offsetTop);
            if (scrollTop >= offsetTop-110) {
                $block.css('marginTop', scrollTop+'px');
            } else {
                $block.css('marginTop', '132px');
            }

        };
        $(window).on('scroll', scrollHandler);
        scrollHandler();
    }
}
/**
  *  Форма расписания
  */
Proj.Form = function(selector){
    var $form = $(selector).find('form');
    var $course = $form.find('select[name=course]');
    var $schedule = $form.find('input[name=schedule]');
    var $date = $form.find('select[name=start_date]');
    $date.prop('disabled', true).parent('.form__form-group').addClass('disabled');
    $date.on('change', function(){
        $schedule.val($date.find(':selected').data('schedule'));
    });
    $course.on('change', function(){
        var course_id = $(this).val();
        Proj.getJSON( { '_do': 'get_schedule_dates', 'course_id': course_id }, function(data) {
            $schedule.val(-1);
    		if(data.status=='ok'){
    		    if (data.html!='') {
                    $date.prop('disabled', false).parent('.form__form-group').removeClass('disabled');
                    $date.html(data.html);
                } else {
                    $date.prop('disabled', true).parent('.form__form-group').addClass('disabled');
                    $date.html('');
                }
            }
        });
    });
}
//------------------------------------------------------------------------------
Proj.Scroll = function() {

    var $body = $("html, body");
    var $top_block = $(".header");

    var offsetTop = $("#top").offset().top;
    var offsetFooter = $("#bottom").offset().top;
    var wHeight = window.innerHeight;
    var lHeight = $top_block.outerHeight();

    _initScroll();

    function _initScroll() {
        var $topLink = $(".on-top").click(function(e) {
            $body.animate({ scrollTop: 0 }, 600);
            return false;
        });

        function _scrollHandler(e) {
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop;

            if (scrollTop > lHeight) {
                $topLink.fadeIn();
                if (scrollTop+wHeight > offsetFooter) {
                    var y = scrollTop + wHeight - offsetFooter;
                    $topLink.css('bottom', y+'px !important');
                } else {
                    $topLink.css('bottom', 0+'px !important');
                }
            } else {
                $topLink.fadeOut();
            }
        };

        $(window).on('scroll', _scrollHandler);
        _scrollHandler();
    }
}
//------------------------------------------------------------------------------
Proj.Mobile = function() {
    var $top_menu = $(".top-menu").find('.is--arrow');
    $top_menu.on('click', function(){
        //console.log('Click');
        var down = $(this).hasClass('m--down');
        var $submenu = $(this).siblings('.header__navbar-sub');
        //console.log(down);
        if (down) {
            $(this).removeClass('m--down').addClass('m--up');
            $submenu.slideDown();
        } else {
            $(this).removeClass('m--up').addClass('m--down');
            $submenu.slideUp();
        }
    });
    var $levels = $(".levels").find('.js-level');
    $levels.on('click', function(){
        //console.log('Click');
        var active = $(this).hasClass('m--active');
        var $submenu = $(this).siblings('.levels__dropbox');
        //console.log(down);
        if (active) {
            $(this).removeClass('m--active');
            $submenu.slideUp();
        } else {
            $(this).addClass('m--active');
            $submenu.slideDown();
        }
        return false;
    });
}
//------------------------------------------------------------------------------
Proj.Modal = function() {
    $('#modal').on('show.bs.modal', function (e) {
       	$('#modal .modal__title').text('');
    	$('#modal .modal__body').html('');
        if (e.relatedTarget) {
            var $related = $(e.relatedTarget);
            var class_name = $related.data('class_name');
            var object_id = $related.data('object_id');
            var course = $related.data('course');
            var schedule = $related.data('schedule');
            var template = $related.data('template');
            var style = $related.data('style');
        } else {
            var class_name = e.currentTarget.dataset.class_name;
            var object_id = e.currentTarget.dataset.object_id;
            var course = e.currentTarget.dataset.course;
            var schedule = e.currentTarget.dataset.schedule;
            var template = e.currentTarget.dataset.template;
            var style = e.currentTarget.dataset.style;
        }
        $('#modal .modal-dialog').addClass(style);
        Proj.getJSON( { '_do': 'get_'+class_name, 'object_id': object_id, 'schedule': schedule, 'course': course }, function(data) {
            $('#modal .modal__title').html(data.title);
            $('#modal .modal__body').html(data.html);
            if (class_name=='page_with_form') {
                Proj.Form('.modal');
        		var $form = $('#modal .modal__body').find('form');
          	    $form.find(".form__form-group").each(function() {
          	        var $input = $("input[type='text'], textarea", this);
          	        if ($input.length) {
          	            var text = $(".form__help-block", this).hide().text();
          	            $input.attr({placeholder: text});
          	        }
          	    });

        		$form.ajaxForm({
        			beforeSend: function(){
        				$form.find(".has-error").removeClass("has-error");
        			},
        			success: function(e){
        				answer(e);
        			},
        			error: function(e){
        			},
                    type: 'POST',
        			dataType: 'json',
        			url: '/json/',
        			data: { _do: 'submit_form' }
        		});
			}
        });
      	function answer(json) {
      		var $form = $('#modal .modal__body').find('form');
    		if(json.status=="error"){
      			if(json.errors){
    				for(var i=0;i<json.errors.length;i++){
      					var attr = ".attr-"+json.errors[i];
      					$form.find(attr).addClass("has-error");
    				}
                }
  			}else{
				var $cblock = $('#modal .modal__body');
				$cblock.html(json.content);
  			}
  	    }
        $('#modal').on('hidden.bs.modal', function (e) {
            $('#modal .modal__dialog').removeClass(style);
        });
    });
}

Proj.ProdForm = function() {

    function InitForm(form) {

        var $form = $(form);

        $form.ajaxForm({
            beforeSend: function(){
                $form.find(".has-error").removeClass("has-error");
            },
            success: function(e){
                answer(e,form);
            },
            error: function(e){
            },
            type: 'POST',
            dataType: 'json',
            url: '/json/',
            data: { _do: 'submit_form' }
        });

        function answer(json,form) {
            var $form = $('form');
            if(json.status=="error"){
                if(json.errors){
                    for(var i=0;i<json.errors.length;i++){
                        var attr = ".attr-"+json.errors[i];
                        $form.find(attr).addClass("has-error");
                    }
                }
            }else{
                $form.html(json.content);
            }
        }
    }

    
    $('.js-prod-form').each(function() {
        InitForm(this); 
    });
}
//------------------------------------------------------------------------------

Proj.HomeCourseMore = function() {
    var opt = {
        moreSelector: '.js-course-more',
        itemSelector: '.js-course-item',
        duration: 400
    };

    var $more = $(opt.moreSelector),
        $items = $(opt.itemSelector),
        count = parseInt($more.data('count'));

    for (var i = count; i < $items.length; i ++) {
        $items.eq(i).hide();
    }

    $more.on('click', function(e) {
        e.preventDefault();

        for (var i = count; i < $items.length; i ++) {
            $items.eq(i).slideDown(opt.duration);
        }

        $more.hide();
    });
    
};

Proj.HomeMenuXs = function() {
    
    var opt = {
        menuSelector: '.js-menu-xs',
    };

    var pxl = 100
    var $menuElem = $('#navbar_xs');

    $(window).on('scroll', function(e) {

        var top = $(window).scrollTop();

        if (top <= pxl) {
            $menuElem.removeClass('navbar_xs');
        } else {
            $menuElem.addClass('navbar_xs');
        }
    });

};

Proj.FixedBanner = function() {
    var opt = {
        selector: '.js-fixed-banner',
        closeSelector: '.js-fixed-banner__close'
    };

    var $block = $(opt.selector),
        $close = $block.find(opt.closeSelector),
        count = parseInt($block.data('count'));

    $block.click(function() {
        $block.css( 'display', 'none');
    });

    var value = Cookies.get('showFixedBaner');
    if (value) {
        value = parseInt(value);

        if (value > count) {
            $block.css( 'display', 'none');
        } else {
            $block.css( 'display', 'block');
        }

        Cookies.set('showFixedBaner', value + 1, '+7d', '/');
    } else {
        $block.css( 'display', 'block');

        Cookies.set('showFixedBaner', 1, '+7d', '/');
    }
};
