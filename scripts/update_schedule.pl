#!/usr/bin/perl

use utf8;
use strict;
use warnings;

use lib '/usr/local/flexites/lib';
use lib '/usr/local/flexites/projects';

use POSIX;

use Engine::GlobalVars;
use Engine::Config;
use Engine::Application;

use kafedramody::dblib::Database;

use Data::Dumper;

use Encode;

$project = 'kafedramody';
$prefix = '/usr/local/flexites';
$path = $prefix . '/projects/' . $project;

my $pid_file = $path . '/var/update_schedule.pid';

die "Script already running" unless (PIDFileCheck($pid_file));

my $log_file = $path . '/var/update_schedule.log';
our $LOG;
open($LOG, '>:utf8', $log_file) || die "Can't open log file $log_file";
select($LOG);
$SIG{'__DIE__'} = $SIG{'TERM'} = $SIG{'INT'} = \&Finish;

print "=================================================================\n";
print "Обновление базы: ", strftime('%d.%m.%Y %H:%M:%S', localtime()), "\n";
#print encode ( 'utf-8', "Обновление базы: " . strftime('%d.%m.%Y %H:%M:%S', localtime()) . "\n" );



$Conf = new Engine::Config;
$Conf->LoadFromFile($prefix . '/conf/' . $project . '.conf');

$DB = new kafedramody::dblib::Database;
$DB->Connect(
    $Conf->Param('database/dsn'),
    $Conf->Param('database/host'),
    $Conf->Param('database/username'),
    $Conf->Param('database/password')
);
$DB->DatabaseVersion($Conf->Param('database/database_version'));
$App = new Engine::Application;

#my $cur_month = $DB->GetRow(qq{SELECT MONTH(NOW())});
my $cur_year = $DB->GetRow(qq{SELECT YEAR(NOW())});
#my $next_start_date = $DB->GetRow(qq{SELECT LAST_DAY(NOW()) + INTERVAL 1 MONTH});
my $next_start_date = $DB->GetRow(qq{SELECT LAST_DAY(NOW())});
print "YEAR: ".$cur_year."\n";
#print "MONTH: ".$cur_month."\n";
#if ($cur_month == 12) {
#    $next_start_date = $DB->GetRow(qq{SELECT LAST_DAY(NOW())});
#}
my $sc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(class_name=>'schedule')->fetchrow_hashref;
my $condition = qq{YEAR(start_date) = $cur_year AND recruitment};
print "[CONDITION]: ".$condition."\n";
my $schedules = $DB->CAT_MEGAGET(parent_id=>$sc->{'object_id'}, class_name=>'schedule_course', attrs=>['start_date','recruitment'], condition => $condition, recursive => 1)->fetchall_arrayref({});
print "NEXT START DATE: ".$next_start_date."\n";
foreach (@{$schedules}) {
    $DB->CAT_SET_OBJECT_ATTR(object_id=>$_->{'object_id'}, attr_name=>'start_date', value=>$next_start_date);
}

print "Завершено успешно: ", strftime('%d.%m.%Y %H:%M:%S', localtime()), "\n";
#print encode ( 'utf-8', "Завершено успешно: " . strftime('%d.%m.%Y %H:%M:%S', localtime()) . "\n" );
Finish();

sub PIDFileCheck {
    my $pidfile = shift;
    return 0 if (-f $pidfile);
    my $fh;
    open($fh, '>', $pidfile) || die "Cant't open PID file $pidfile";
    print $fh $$;
    close($fh);
    return 1;
}

sub Finish {
    my $why = shift;
    my $stack = 1;

    for (my $stack = 1;  my $s = (CORE::caller($stack))[3];  $stack++) {
        if ($s eq '(eval)') {
            return;
        }
    }

    print "[ ERROR ] $why\n" if ($why);
    undef $App;
    undef $DB;
    undef $import;
    close $LOG;
    unlink $pid_file;

    if ($why) {
        exit 1;
    }
    exit;
}
