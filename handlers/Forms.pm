package handlers::Forms;

use strict;
use utf8;

use base qw(Handlers::Forms);

use Engine::GlobalVars;
use Engine::SysUtils;

use Encode;

use Data::Dumper;


sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless($self, $class);

    return $self;
}

# Универсальная отправка формы.
sub StandartForm {
    my $self = shift;
    my (%params) = @_;

    my $current_time = $DB->GetRow(qq{SELECT NOW()});

    my $form = $DB->CAT_GET_OBJECT_WITH_ATTRS(
        object_id => $App->Param('form_id'),
        attrs     => ['_form_class_name', '_create_container', '_mail_template', '_mail_subject', '_recipient_mail_address', '_mail_encoding']
    )->fetchrow_hashref();

    if ($form) {
        my $form_name = $form->{'_form_class_name'};

        my $captcha_test = $self->CaptchaTest($App->Param('__captcha_answer'),$form->{'_form_class_name'});

        my ($values, $errors, $attrs) = $self->ExtractAttrsFromApp(
            form_name => $form_name,
            attrs     => {
                publication   => 0,
                addition_date => $current_time
            }
        );

        if ((!%$errors) && ($captcha_test)) {
            if ($form->{'_create_container'}) {
                my $new_object_name = $current_time;
                $new_object_name .= " : ".$App->Param('node_name') if (defined $App->Param('node_name'));
                $new_object_name .= " : ".$App->Param('person') if (defined $App->Param('person'));

                my $parent_id = $self->GetParentId($form);
                $parent_id = $App->Param('course') if ($App->Param('course') and $App->Param('course') > 0);
                $parent_id = $App->Param('schedule') if ($App->Param('schedule') and $App->Param('schedule') > 0);

                $self->CreateObject(
                    class_name  => $form_name,
                    object_name => $new_object_name,
                    parent_id   => $parent_id,
                    values      => $self->ProceedValues($form,$values)
                )
            }

            my $mail_hash = $self->GetMailHash($form);

            $mail_hash->{'FORM_VALUES'} = $attrs;

            $mail_hash->{'mail_reply_to'} = $values->{'email'}->{'value'} if ($values->{'email'});

            $self->Mailer->SendMail(
                template => $form->{'_mail_template'} || 'default',
                encoding => $form->{'_mail_encoding'},
                hash     => $mail_hash
            );

            $self->AdditionalNotification(form_id=>$form->{'object_id'},
                                          form_class => $form->{'_form_class_name'},
                                          attrs => $attrs);

            $App->Session->Var('FORMS_' . $form_name, 'SENT');
            if (!$App->Data->Var('__AJAX_REQUEST')) {
                my $url = ($App->Param('__BACK'))
                    ? $App->Param('__BACK')
                    : ConstructUrlString('', { node_id=>$App->Param('node_id') });
                $App->RedirectTo($url);
            }
            return 1;
        } else {
            $App->Param($form_name . '_error', 1);
            $App->Data->Var($form_name . '_errors', $errors);
            $App->Data->Var('__CAPTCHA_ERROR', 1) if (!$captcha_test);
            return -2;
        }
    } else {
        return -1;
    }
}

# Извлечение данных из App->Param
sub ExtractAttrsFromApp {
    my $self = shift;
    my (%params) = (
        group_name => 'display',
        group_name_additional => 'additional',
        attrs      => {},
        @_
    );
    my @attrs;

    my $values = $DB->CAT_EXTRACT_ATTRS_FROM_APP(class_name => $params{'form_name'});
    my %errorsd = $DB->CAT_CHECK_OBJECT_ATTRS(
        class_name      => $params{'form_name'},
        attr_group_name => $params{'group_name'},
        values          => $values
    );

    my %errorsa = $DB->CAT_CHECK_OBJECT_ATTRS(
        class_name      => $params{'form_name'},
        attr_group_name => $params{'group_name_additional'},
        values          => $values
    );

    my %errors = (%errorsd, %errorsa);
    if (!%errors) {
        my $attrs_sql = $DB->CAT_GET_CLASS_ATTRS(class_name => $params{'form_name'});
        while (my $attr = $attrs_sql->fetchrow_hashref()) {
            if (defined $params{'attrs'}->{ $attr->{'attr_name'} }) {
                $values->{ $attr->{'attr_name'} } = { value => $params{'attrs'}->{ $attr->{'attr_name'} } };
                $attr->{'value'} = $params{'attrs'}->{ $attr->{'attr_name'} };
            } else {
                $attr->{'value'} = $values->{ $attr->{'attr_name'} }->{'value'}
                    if ($values->{ $attr->{'attr_name'} });
            }
            push @attrs, $attr if (defined $attr->{'value'});
        }
    } else {
        my $attrs_sql = $DB->CAT_GET_CLASS_ATTRS(class_name => $params{'form_name'}, attr_group_name => $params{'group_name'});
        while (my $attr = $attrs_sql->fetchrow_hashref()) {
            my $val = $values->{ $attr->{'attr_name'} }->{'value'};
            if ((($attr->{'field_type'} eq 'date') || ($attr->{'field_type'} eq 'date_time')) && (defined $val)) {
                $App->Param($attr->{'attr_name'}, $val);
            }
        }
        my $attrs_sql = $DB->CAT_GET_CLASS_ATTRS(class_name => $params{'form_name'}, attr_group_name => $params{'group_name_additional'});
        while (my $attr = $attrs_sql->fetchrow_hashref()) {
            my $val = $values->{ $attr->{'attr_name'} }->{'value'};
            if ((($attr->{'field_type'} eq 'date') || ($attr->{'field_type'} eq 'date_time')) && (defined $val)) {
                $App->Param($attr->{'attr_name'}, $val);
            }
        }
    }

    return (
        $values,
        \%errors,
        \@attrs
    );
}

1;
