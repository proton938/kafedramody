/**
 * Gulp-задания для сборщика
 *
 * Необходимые плагины:
 *     gulp
 *     gulp-plumber
 *     path
 *     gulp-less
 *     gulp-concat
 *     gulp-clean-css
 *     gulp-uglify
 *
 * Задания:
 *     watch-less - наблюдатель за изменением LESS файлов и их сборкой
 *
 *     Предполагаема структура директорий:
 *         css/           - папка с css
 *             c/         - папка со склеенным и уменьшенным css
 *             less/      - исходные файлы LESS
 *             maps/      - папка с map файлами для css
 *
 *     Настройки имен файлов и директорий ниже
 */

var gulp = require('gulp'),                       // Сборщик Gulp
    plumber = require('gulp-plumber'),            // Обработчик ошибок сборщика
    path = require('path'),                       // Утилита для работы с путями
    less = require('gulp-less'),                  // Компилятор LESS
    watchLess = require('gulp-watch-less'),       // Наблюдатель за всеми includa'ми в файле
    concat = require('gulp-concat'),              // объединяет файлы в один бандл
    cleanCSS = require('gulp-clean-css'),         // сжимает css
    uglify = require('gulp-uglify'),              // Сжимает js
    rename = require('gulp-rename'),              // Переименовывает
    sourcemaps = require('gulp-sourcemaps');      // Генерация Source-maps


// ---------------- CSS -------------------------------------------------

var cssPath = './css/';                               // Директория, где располагаются CSS-файлы
var cssMapPath = '/maps/';                            // Директория с map-файлами для css
var cssOutFile = 'style.css';                         // Название выходного файла css
var cssOutFileDev = 'style_dev.css';                         // Название выходного файла css
var cssOutPath = path.join(cssPath, 'c');             // папка со склеенным и уменьшенным css. Файл имеет имя cssOutFile
var cssMapOutPath = path.join('..', cssMapPath, 'c'); // Директория с map-файлами для собранного css

var cssSourcemapsOptions = { sourceMappingURLPrefix: '/css' }; // Опции для настройка sourceMaps

// ---------------- LESS -------------------------------------------------

var lessPath = path.join(cssPath, 'less');        // Папка с LESS файлами
var mainLess = path.join(lessPath, 'style.less'); // Имя главного LESS файла проекта

// Настройки путей для LESS
var lessConfig = {
    javascriptEnabled: true,
    paths: [
        lessPath,
        path.join(lessPath, 'parts'),
        path.join(lessPath, 'inc')
    ],
};


var onError = function (err) {
    console.error(err);
};

// Наблюдатель за less файлами
gulp.task('watch-less', function() {
    const watcher = gulp.watch(path.join(lessPath, '**', '*.less'), { awaitWriteFinish: true });

    watcher.on('change', function(path) {
        console.log(path, 'was changed');

        gulp.src(mainLess)
            .pipe(sourcemaps.init())
            .pipe(plumber({
                errorHandler: onError
            }))
            .pipe(less(lessConfig))
            .pipe(plumber.stop())
            .pipe(cleanCSS({ keepBreaks: true, restructuring: false }))
            .pipe(rename(cssOutFile))
            .pipe(sourcemaps.write(cssMapPath, cssSourcemapsOptions))
            .pipe(gulp.dest(cssPath));
    });
});

// Сборка и минимизация CSS из LESS
gulp.task('build-less', function(done) {
    gulp.src(mainLess)
        .pipe(sourcemaps.init())
        .pipe(less(lessConfig))
        .pipe(cleanCSS())
        .pipe(rename(cssOutFile))
        .pipe(sourcemaps.write(cssMapOutPath, cssSourcemapsOptions))
        .pipe(gulp.dest(cssOutPath))
        .on('end', done);
});

// Сборка проекта
//gulp.task('build', ['build-less']);
gulp.task('build', gulp.series('build-less'));
