package kafedramody::actions::json;

#
# Пакет для работы с ajax запросами
# Данные возвращаются в формате JSON
#

use strict;
use utf8;

use base qw(Engine::Action);

use Engine::GlobalVars;
use Engine::SysUtils;
use Engine::Helpers;
use Data::Dumper;

use base qw(kafedramody::controllers::__site__);
#use kuizo::controllers::templates;
use kafedramody::controllers::page_with_form;

use JSON;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);

	bless($self, $class);

	return $self;
}


sub Init {
	my $self = shift;
	$self->{'_RETVAL'} = {};

	my $action = $App->Param('_do');

	my $act = 'H_' . $action;
	my $r = {};
	eval {
		$r = $self->$act;
	};
	if ($@) {
		print "ERROR ", $@, "\n";
		$r = {};
		$self->Status(404);
	}

	$self->RetVal($r);
}

#-------------------------------------------------------

sub H_get_schedule_dates {
	my $self = shift;
	my $r = { status => 'error', html => '' };

    if ($App->Param('course_id')) {
        my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'),
    				                class_name => 'schedule')->fetchrow_hashref();
    	my $schedules = $DB->CAT_MEGAGET(parent_id=>$p->{'object_id'},
    							   class_name=>'schedule_course',
    							   attrs=>['subject','all_hours','times_of_day','start_date','time_of_training','addition','total_price','publication'],
                                   condition => qq{publication AND start_date >= NOW() AND subject IN(:course_id)},
       							   recursive => 1,
                                   order_by => [['start_date','ASC']],
                                   course_id => $App->Param('course_id'));
    	while(my $row = $schedules->fetchrow_hashref){
     		$row->{'date'} = Engine::Helpers::DateFormater->new(date => $row->{'start_date'});
    	    $r->{'html'} .= q{<option data-schedule="}.$row->{'object_id'}.q{">}.$row->{'date'}->html('%e %RU_MONTH %Y').q{</option>};
        }
        $r->{'html'} = q{<option data-schedule="-1"></option>}.$r->{'html'} if ($r->{'html'} ne '');
        $r->{'status'} = 'ok';
    	return $r;
    }
}

#-------------------------------------------------------

sub H_set_filter {
	my $self = shift;
	my $r = { status => 'error' };
	my @filter_addresses = split(',', $App->Session->Var('_FILTER_ADDRESSES'));
    if ($App->Param('addresses')) {
        $App->Session->Var('_FILTER_ADDRESSES', $App->Param('addresses'));
        @filter_addresses = split(',', $App->Param('addresses'));
    } else {
        $App->Session->UndefVar('_FILTER_ADDRESSES');
    }
	my @filter_letters = split(',', $App->Session->Var('_FILTER_LETTERS'));
    if ($App->Param('letters')) {
        $App->Session->Var('_FILTER_LETTERS', $App->Param('letters'));
        @filter_letters = split(',', $App->Param('letters'));
    } else {
        $App->Session->UndefVar('_FILTER_LETTERS');
    }
	my @filter_times = split(',', $App->Session->Var('_FILTER_TIMES'));
    if ($App->Param('times')) {
        $App->Session->Var('_FILTER_TIMES', $App->Param('times'));
        @filter_times = split(',', $App->Param('times'));
    } else {
        $App->Session->UndefVar('_FILTER_TIMES');
    }
    $r->{'status'} = 'ok';
	return $r;
}

#-------------------------------------------------------

sub H_set_show_type {
	my $self = shift;
	my $r = { status => 'error' };
    if ($App->Param('type')) {
        $App->Session->Var('_SHOW_TYPE', $App->Param('type'));
        $r->{'status'} = 'ok';
    }
	return $r;
}

#-------------------------------------------------------

sub H_get_page {
	my $self = shift;
	my $r = { status => 'error' };

    if (!$App->Param('node_id')) {
        my $h = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(class_name=>'home_page')->fetchrow_hashref;
        $App->Param('node_id', $h->{'object_id'});
    }
	my $object = $DB->CAT_GET_OBJECT(object_id => $App->Param('node_id'))->fetchrow_hashref;
	my $controller = new kafedramody::controllers::templates();
    my $act = '_get_'.$object->{'class_name'};
	my $template = $controller->$act;
	my ($content, $error) = $controller->Templates->Content(
		template => 'ajax/'.$object->{'class_name'}.'.tx',
		data     => $template
	);
	if(!$error){
    	my $controller = new kuizo::controllers::__site__();
    	my $template = {
        	    main_menu => $controller->_getMainMenu(),
                _interface_name => $object->{'class_name'}
            };
    	my ($navi, $error) = $controller->Templates->Content(
    		template => 'includes/yellow_triangle.tx',
    		data     => $template
    	);
		$r = {
		    html => $content,
		    navi => $navi,
            interface_name => $object->{'class_name'},
            status => 'ok'
        };
	}

	return $r;
}

#-------------------------------------------------------

sub H_get_page_with_form {
	my $self = shift;
	my $r = { content => 'error' };

	if(($App->Param('form_name'))&&(my $form = $DB->CAT_MEGAGET(class_name=>'page_with_form',
																attrs=>['_form_class_name'],
																condition=>qq{ _form_class_name = :form_name },
																form_name=>$App->Param('form_name'),
																limit_rows=>1)->fetchrow_hashref())){
		$App->Param('object_id',$form->{'object_id'});
	}

	if($App->Param('object_id')){
		$App->Data->Var('__AJAX_REQUEST', 1);

		if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('object_id'),
													attrs => ['_content','_s_content','_form_class_name'])->fetchrow_hashref()){
			my $pwf = new kafedramody::controllers::page_with_form();

            if ($App->Param('item_id')) {
                my $i = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('item_id'), attrs=>['length','label_txt'])->fetchrow_hashref;
                $obj->{'item_id'} = $App->Param('item_id');
                $obj->{'item_name'} = $i->{'label_txt'};
                $obj->{'form_name'} .= ' <span class="area">'.$i->{'object_name'}.'<span>2</span></span>';
            }
			my $form = $pwf->ConstructForm($obj);
            my @hidden = ();
            if ($obj->{'_form_class_name'} eq 'review') {
                if ($App->Param('course')>0) {
                    my $course = $DB->CAT_GET_OBJECT(object_id=>$App->Param('course'))->fetchrow_hashref;
                    $course->{'href'} = ConstructURL(node_id=>$course->{'object_id'});
                    $course->{'name'} = 'course';
                    push @hidden, $course;
                    $form->{'parent'} = $course;
                }
            }
            if ($obj->{'_form_class_name'} eq 'form_course') {
                if ($App->Param('schedule')>0) {
                    my $schedule = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('schedule'), attrs=>['subject','start_date','recruitment'])->fetchrow_hashref;
                    $schedule->{'href'} = ConstructURL(node_id=>$schedule->{'object_id'});
                    $schedule->{'name'} = 'schedule';
            		$schedule->{'date'} = Engine::Helpers::DateFormater->new(date => $schedule->{'start_date'});
                    push @hidden, $schedule;
                    my $course = $DB->CAT_GET_OBJECT(object_id=>$schedule->{'subject'})->fetchrow_hashref;
                    $form->{'parent'} = $course;
                    $form->{'schedule'} = $schedule;
                    push @hidden, {
                        name => 'start_date',
                        object_id => $schedule->{'start_date'}
                    };
                    $course->{'name'} = 'course';
                    push @hidden, $course;
                } else {
        			$pwf->AdditionalInfo();
                    push @hidden, {
                        name => 'schedule',
                        object_id => ''
                    };
                }
            }

            $form->{'hidden'} = \@hidden;

			$pwf->Data({ object => $obj,
						 form => $form });
            if ($obj->{'_form_class_name'} eq 'form_course' && !defined($App->Param('schedule'))) {
			    $pwf->AdditionalInfo();
            }
            if ($obj->{'_form_class_name'} eq 'review' && !defined($App->Param('course'))) {
			    $pwf->AdditionalInfo();
            }
			my ($content, $error) = $pwf->Templates->Content(
				template => "ajax/page_with_form.tx",
				data     => { object => $obj,
							  form => $form,
                              course => $App->Param('course')}
			);
			if(!$error){
				$r = { html => $content,
					   object_id => $obj->{'object_id'},
					   title => $obj->{'form_name'} || $obj->{'object_name'} };
			}
			$pwf->DESTROY();
		}
	}

	return $r;
}

#-------------------------------------------------------

sub H_submit_form {
	my $self = shift;
	my $r = {};
	if($App->Param('form_id')){
		$App->Data->Var('__AJAX_REQUEST', 1);
		my $form_name = $DB->CAT_GET_OBJECT_ATTR(object_id => $App->Param('form_id'), attr_name => '_form_class_name');

		my $pwf = new kafedramody::controllers::page_with_form();
		my $answer = $pwf->Submit();
		if(!$answer){
			my $errors = $App->Data->Var($form_name . '_errors');
			$r->{'type'} = 'submit_form';
			if ($errors) {
				$r->{'status'} = 'error';
				my @err;
				foreach (keys %$errors) {
					push @err, $DB->GetRow(qq{SELECT `name` FROM `T_##_CAT_CLASSES_ATTRS` WHERE id = :id}, { id => $_ });
				}
				$r->{'errors'} = \@err;
			}
		}else{
			$r->{'status'} = 'ok';
			$r->{'content'} = $DB->CAT_GET_OBJECT_ATTR(object_id => $App->Param('form_id'), attr_name => '_s_content');
		}
		$App->Session->UndefVar('FORMS_' . $form_name);
		$pwf->DESTROY();
	}else{
		$r->{'status'} = 'error';
	}
	return $r;
}

#-------------------------------------------------------

sub H_submit_fform {
	my $self = shift;
	my $r = {};
	if($App->Param('form_id')){
   		$App->Data->Var('__AJAX_REQUEST', 1);
		my $form_name = $DB->CAT_GET_OBJECT_ATTR(object_id => $App->Param('form_id'), attr_name => '_form_class_name');

		my $pwf = new kuizo::controllers::page_with_form();
		my $answer = $pwf->Submit();
        $App->Data->UndefVar('__AJAX_REQUEST');
        my $captcha_test = $self->CaptchaTest($App->Param('__captcha_answer'));

		if((!$answer) || (!$captcha_test)){
			my $errors = $App->Data->Var($form_name . '_errors');
			$r->{'type'} = 'submit_form';
			if (($errors) || (!$captcha_test)) {
				$r->{'status'} = 'error';
				my @err;
				foreach (keys %$errors) {
					push @err, $DB->GetRow(qq{SELECT `name` FROM `T_##_CAT_CLASSES_ATTRS` WHERE id = :id}, { id => $_ });
				}
                push @err, 'captcha' if (!$captcha_test);
        		$r->{'captcha'} = CreateCaptcha(color1 => "#666666",
					                    color2 => "#cccccc",
										type => 2);
				$r->{'errors'} = \@err;
			}
		}else{
			$r->{'status'} = 'ok';
			$r->{'content'} = $DB->CAT_GET_OBJECT_ATTR(object_id => $App->Param('form_id'), attr_name => '_s_content');
		}
		$App->Session->UndefVar('FORMS_' . $form_name);
		$pwf->DESTROY();
	}else{
		$r->{'status'} = 'error';
	}
	return $r;
}

#-------------------------------------------------------

sub CaptchaTest {
    my $self = shift;
    my ($value) = @_;
    my $ret = ((($value) && ($App->Session->Var('__CAPTCHA_SECRET')) && ($App->Session->Var('__CAPTCHA_SECRET') eq $value)));
    $App->Session->UndefVar('__CAPTCHA_SECRET') unless (($ret));
    return $ret;
}

#-------------------------------------------------------

sub Run {
	my $self = shift;

	#$App->ContentType('application/json');
  $App->ContentType('text/plain');
	$self->Text(encode_json($self->RetVal));
}

#-------------------------------------------------------

sub RetVal {
	my $self = shift;
	$self->{'_RETVAL'} = shift if (@_);
	return $self->{'_RETVAL'};
}

1;
__END__
