package kafedramody::actions::index;

use strict;

use base qw(Actions::index);

use Engine::GlobalVars;
use Engine::SysUtils;
use Engine::Helpers;
use Time::HiRes qw(gettimeofday);
use Data::Dumper;

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    
    my $self = $class->SUPER::new(@_);
    return $self;
}

sub Init {
    my $self = shift;

    if ( defined($App->Param('test_quit')) ) {
        $App->Session->UndefVar('test_mode');
    } elsif ( defined($App->Param('test_mode')) or defined($App->Session->Var('test_mode')) ) {
        $App->Session->Var('test_mode', 1);
        $App->Param('__mobile_version', 1);
    }
    return $self->SUPER::Init(@_);
}

sub DefineDefaults {
    my $self = shift;
    $self->SUPER::DefineDefaults(@_);

    if(!$App->Param('__mobile_version') && (IsMobile())){
        $App->Param('__mobile_version', 1);
    }
}

1;
