package kafedramody::controllers::programs_group;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>[])->fetchrow_hashref()){
		$data->{'object'} = $obj;
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
	}
    $data->{'programs'} = $self->_getPrograms();
}

=com
	Получение программ по группам
=cut
sub _getPrograms {
	my $self = shift;
	my @groups = ();

	if(my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'),
												    class_name => 'type_of_educational_program_cat')->fetchrow_hashref()){

    	my $groups = $DB->CAT_MEGAGET(parent_id=>$p->{'object_id'},
    							   class_name=>'type_of_educational_program',
                                   attrs=>[],
                                   use_publ_pos => 1);
        print Dumper($groups);
    	while(my $row = $groups->fetchrow_hashref){
            my $condition = qq{publication};
            $condition .= qq{ AND type_of_educational_program = $row->{'object_id'}};
            print "CONDITION: ".$condition."\n";
        	my @items = ();
        	my $pages = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'),
        							   class_name=>'program_page',
        							   attrs=>['picture','annotation','position','publication','type_of_educational_program'],
                                       condition => $condition,
                                       recursive => 1,
           							   order_by => [['position','ASC']]);

        	while(my $page = $pages->fetchrow_hashref){
    			$page->{'href'} = ConstructURL(node_id=>$page->{'object_id'});
        		$page->{'icon'} = GetImage(file_id=>$page->{'picture'});

                push (@items, {
                    page => $page,
                });
            }
            if (@items > 0) {
                $row->{'items'} = \@items;
                push(@groups, $row);
            }
        }
    }

	return {
	    groups => \@groups,
        program_message => $App->Settings->GetParamValue('Messages.NoProgram'),
    };
}

1;
__END__
