package kuizo::controllers::templates;

use utf8;
use strict;

use base qw(kuizo::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
}

sub _get_page{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>['content'])->fetchrow_hashref()){
		$data->{'object'} = $obj;
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
        $data->{'main_menu'} = $self->_getMainMenu();
	}
    return $data;
}

sub _get_home_page{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>['content'])->fetchrow_hashref()){
		$data->{'object'} = $obj;
        $data->{'main_menu'} = $self->_getMainMenu();
	}

    return $data;
}

1;
__END__
