package kafedramody::controllers::news_cat;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>[])->fetchrow_hashref()){
		$obj->{'object_name'} = TextCut($obj->{'object_name'}, { removehtml => 1 });
		$data->{'object'} = $obj;
	}
    $data->{'newses'} = $self->_getNewses();
    #$data->{'filter'} = $self->_getFilter();
    #print Dumper($data);
}

=com
	Получение курсов направления по группам
=cut
sub _getNewses {
	my $self = shift;

	my @items = ();
	my $pages = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'),
							   class_name=>'news',
							   attrs=>['publication_date','content'],
 							   order_by => [['publication_date','DESC']]);

	while(my $page = $pages->fetchrow_hashref){
        $page->{'href'} = ConstructURL(node_id=>$page->{'object_id'});
        $page->{'publication_date'} = Engine::Helpers::DateFormater->new(date => $page->{'publication_date'});
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$page->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $page->{'gallery'} = \@gallery;
        }

        push (@items, $page);
  }
  return \@items;
}

1;
__END__
