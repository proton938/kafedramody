package kafedramody::controllers::page_with_form;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Data::Dumper;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use kafedramody::handlers::Forms;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;

	if(!$self->Submit()){

		$self->SUPER::Init(@_);

		my $data = $self->Data();

		if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
													attrs => ['_content','_s_content','_form_class_name'])->fetchrow_hashref()){
			$data->{'object'} = $obj;

			if(($App->Session->Var('FORMS_' . $obj->{'_form_class_name'}) eq 'SENT') and (!$App->Param('form_id'))){
				$data->{'object'}->{'_sent'} = 1;
				$App->Session->UndefVar('FORMS_' . $obj->{'_form_class_name'});
			}else{
				if($obj->{'_form_class_name'}){
					$data->{'form'} = $self->ConstructForm($obj);
                    my @hidden = ();
                    if ($obj->{'_form_class_name'} eq 'review') {
                        if ($App->Param('course')>0) {
                            my $course = $DB->CAT_GET_OBJECT(object_id=>$App->Param('course'))->fetchrow_hashref;
                            $course->{'href'} = ConstructURL(node_id=>$course->{'object_id'});
                            $course->{'name'} = 'course';
                            push @hidden, $course;
                            $data->{'form'}->{'parent'} = $course;
                        } else {
                			$self->AdditionalInfo();
                        }
                    }
                    if ($obj->{'_form_class_name'} eq 'form_course') {
                        if ($App->Param('schedule')>0) {
                            my $schedule = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('schedule'), attrs=>['subject','start_date'])->fetchrow_hashref;
                            $schedule->{'href'} = ConstructURL(node_id=>$schedule->{'object_id'});
                            $schedule->{'name'} = 'schedule';
                    		$schedule->{'date'} = Engine::Helpers::DateFormater->new(date => $schedule->{'start_date'});
                            push @hidden, $schedule;
                            my $course = $DB->CAT_GET_OBJECT(object_id=>$schedule->{'subject'})->fetchrow_hashref;
                            $data->{'form'}->{'parent'} = $course;
                            $data->{'form'}->{'schedule'} = $schedule;
                            push @hidden, {
                                name => 'start_date',
                                object_id => $schedule->{'start_date'}
                            };
                            $course->{'name'} = 'course';
                            push @hidden, $course;
                        } else {
                			$self->AdditionalInfo();
                            push @hidden, {
                                name => 'schedule',
                                object_id => ''
                            };
                        }
                    }
                    $data->{'form'}->{'hidden'} = \@hidden;
				}
			}
		}else{
			$self->can_render(0);
		}
	}else{
		$self->can_render(0);
	}
}

sub ConstructForm{
	my $self = shift;
	my $obj = shift;

	# Функция преобработки поля формы
	my $preprocess = sub() {
		my $input = shift; # input
		my $field = shift; # field описание поля, для которого делаем input

		#print Dumper($input);
		#print Dumper($field);

		# Сюда передается одиночное поле. Не массив. Перебирать что-то через foreach бесмысленно
		# Достаточно написать просто
		$input->{'class'} = 'form__form-control';
		$input->{'placeholder'} = $field->{'publ_comment'};
		$input->{'placeholder'} .= ($field->{'_required'} && $field->{'publ_comment'}) ? '*' : '';
		return $input;
	};

	my $form = {
				action  => ConstructURL( node_id => $obj->{'object_id'} ),
				form_id => $obj->{'object_id'},
				_action => 'send_form',
				_form_class_name => $obj->{'_form_class_name'},
				fields  => GetForm(object_id => $obj->{'item_id'}, form_class  => $obj->{'_form_class_name'},
								   attrs_group => 'display',
								   class => 'form__form-control',
								   preprocess => $preprocess),
				submit_text => $App->Settings->GetParamValue('Forms.SubmitLabel'),
				remark_text => $App->Settings->GetParamValue('Forms.RemarkText'),

			};
	if (!$App->Data->Var('__AJAX_REQUEST')) {
		$form->{'captcha'} = {
								src => CreateCaptcha(color1 => "#993a97",
													 color2 => "#cccccc",
													 type => 2),
								class => ($App->Data->Var('__CAPTCHA_ERROR')) ? 'captcha has-error' : 'captcha',
								captcha_text => $App->Settings->GetParamValue('Forms.CaptchaLabel')
							};
	}
  if (my $agreement = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'), class_name=>'agreement_page', attrs=>['agreement_title','agreement_file','content'], use_publ_pos=>1)->fetchrow_hashref) {
      $form->{'_agreement'} = $agreement;
  }
	return $form;
}

=com
	Дополнительная обработка формы
=cut
sub AdditionalInfo{
	my $self = shift;

	# Установки специального метода для на случай ajax отправки формы
	my $data = $self->Data();

	# Функция преобработки поля формы
	my $preprocess = sub() {
		my $input = shift; # input
		my $field = shift; # field описание поля, для которого делаем input

		#print Dumper($input);
		#print Dumper($field);

		# Сюда передается одиночное поле. Не массив. Перебирать что-то через foreach бесмысленно
		# Достаточно написать просто
		$input->{'class'} = 'form__form-control';
		$input->{'placeholder'} = $field->{'publ_comment'};
		$input->{'placeholder'} .= ($field->{'_required'}) ? '*' : '';
		return $input;
	};
	if($data->{'object'}->{'_form_class_name'} eq 'form_course'){
        my $fields = GetForm(form_class  => $data->{'object'}->{'_form_class_name'},
        				   attrs_group => 'additional',
        				   class => 'form__form-control',
        				   preprocess => $preprocess);
		push $data->{'form'}->{'fields'}, @$fields;
	}
	if($data->{'object'}->{'_form_class_name'} eq 'review'){
        my $fields = GetForm(form_class  => $data->{'object'}->{'_form_class_name'},
        				   attrs_group => 'additional',
        				   class => 'form__form-control',
        				   preprocess => $preprocess);
		push $data->{'form'}->{'fields'}, @$fields;
	}

}

sub Submit{
	my $self = shift;

	my $result = 0;
	if(($App->Param('form_id')) and ($App->Param('_action') eq 'send_form')){
		my $h = handlers::Forms->new();
		my $rs = $h->StandartForm();
		if($rs>0){
			$result = 1;
		}
	}
	return $result;
}

1;
__END__
