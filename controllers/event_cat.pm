package kafedramody::controllers::event_cat;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>[])->fetchrow_hashref()){
		$data->{'object'} = $obj;
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
	}
    $data->{'groups'} = $self->_getGroups();
}

=com
	Получение групп
=cut
sub _getGroups {
	my $self = shift;
	my @items = ();
    my $groups = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'), class_name=>'event_group', attrs=>['picture','annotation'], use_publ_pos=>1);
    while(my $group = $groups->fetchrow_hashref){
  		$group->{'picture'} = GetImage(file_id=>$group->{'picture'});
        $group->{'href'} = ConstructURL(node_id=>$group->{'object_id'});
		$group->{'object_name'} = TextCut($group->{'object_name'}, { removehtml => 1 });
		push(@items, $group);
    }
	return {
	    items => \@items
    };
}

1;
__END__
