package kafedramody::controllers::person_cat;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>['content'])->fetchrow_hashref()){

		$data->{'object'} = $obj;

    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
	}
    $data->{'persons'} = $self->_getPersons();
    #print Dumper($data);
}

=com
	Получение списка сотрудников
=cut
sub _getPersons {
	my $self = shift;
    my @person = ();

	my $persons = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'),
							   class_name=>'person',
							   attrs=>['name','surname','picture','content','activity'],
                               use_publ_pos => 1);

	while(my $row = $persons->fetchrow_hashref){
		$row->{'picture'} = GetImage(file_id=>$row->{'picture'});
        $row->{'href'} = ConstructURL(node_id=>$row->{'object_id'});
		push(@person, $row);
    }
    return \@person;
}
1;
__END__
