package kafedramody::controllers::home_page;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use Data::Dumper;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;

  my $self = $class->SUPER::new(@_);
  bless($self, $class);

  return $self;
}

sub Init {
  my $self = shift;
  $self->SUPER::Init(no_breadcrumbs=>1);

  my $data = $self->Data();

  if (my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(
      object_id => $App->Param('node_id'),
      attrs => ['annotation', 'content', 'bg']
  )->fetchrow_hashref()){
    $obj->{'bg_src'} = $DB->FILES_GET_FILE_HREF(file_id => $obj->{'bg'}) if ($obj->{'bg'} > 0);

    $data->{'object'} = $obj;
    $data->{'slider_menu'} = $self->_getSliderMenu();
    $data->{'schedules'} = $self->_getScheduleOnHome();
    $data->{'advantages'} = $self->_getAdvancedInfo();

    # Для Сергея
    $data->{'test'} = $self->__test({one => 1, two => 2});
  }
}

# Для Сергея
sub __test {
    my $self = shift;
    return $_[0];
}

sub _getSliderMenu {
    my $self = shift;
    my $menu = $DB->GET_MENU_ITEMS(menu_place => 3)->fetchall_arrayref({});
    foreach my $item (@$menu) {
        $item->{'href'} = ConstructURL(node_id => $item->{'node'});
    }
    return $menu;
}

=com
  Получение курсов на главной
=cut
sub _getScheduleOnHome {
  my $self = shift;
  my (%attrs) = @_;
  my @items = ();

    $App->Param('page', 1) unless (defined $App->Param('page'));
    my $page = $App->Param('page');
    my $per_page = $App->Settings->GetParamValue('XCatalog.ScheduleOnHome') || 10;

  if(my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'),
                            class_name => 'schedule')->fetchrow_hashref()){

      my $schedules = $DB->CAT_MEGAGET(
        parent_id=>$p->{'object_id'},
                     class_name=>'schedule_course',
                     attrs=>['subject','adress','all_hours','times_of_day','start_date','time_of_training','addition','total_price','discount','publication','recruitment'],
                                   recursive => 1,
                                   condition => qq{publication AND start_date >= NOW()},
                                   on_page => $per_page,
                                   page_num => $page,
                                   order_by => [['start_date','ASC']]);

        my $pc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'),
                            class_name => 'prod_cat')->fetchrow_hashref();

      my $pages = $DB->CAT_MEGAGET(parent_id=>$pc->{'object_id'},
                     class_name=>'prod_page',
                     attrs=>['level','hours','picture','annotation'],
                                   recursive => 1,
                        use_publ_pos=>1)->fetchall_hashref('object_id');

      my $lc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'level_cat')->fetchrow_hashref;
        my $levels = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level_group', attrs=>['lettered'], use_publ_pos=>1)->fetchall_hashref('object_id');
        my $lettered = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level', attrs=>['lettered'], recursive=>1, use_publ_pos=>1)->fetchall_hashref('object_id');

      while(my $row = $schedules->fetchrow_hashref){
          my @teacher = $DB->CAT_GET_OBJECT_ATTR(object_id=>$row->{'object_id'}, attr_name=>'teacher', result_type=>'value');
          my @array;
          foreach (@teacher) {
              my $t = $DB->CAT_GET_OBJECT(object_id=>$_)->fetchrow_hashref;
              $t->{'href'} = ConstructURL(node_id=>$t->{'object_id'});
              push (@array, $t);
          }
          $row->{'teacher'} = \@array;
            my $day = $DB->CAT_GET_OBJECT(object_id=>$row->{'times_of_day'})->fetchrow_hashref;
            my $address = $DB->CAT_GET_OBJECT(object_id=>$row->{'adress'})->fetchrow_hashref;
            $row->{'times_of_day'} = $day->{'object_name'};
            $row->{'address'} = $address->{'object_name'};

          $row->{'page'} = $pages->{$row->{'subject'}};
          $row->{'page'}->{'level_s'} = $lettered->{$row->{'page'}->{'level'}}->{'parent_id'};
          $row->{'page'}->{'lettered'} = $levels->{$lettered->{$row->{'page'}->{'level'}}->{'parent_id'}}->{'lettered'};
          $row->{'page'}->{'lettered_l'} = $lettered->{$row->{'page'}->{'level'}}->{'lettered'};
          $row->{'page'}->{'level_name'} = $levels->{$lettered->{$row->{'page'}->{'level'}}->{'parent_id'}}->{'object_name'};
          $row->{'page'}->{'level_name_l'} = $lettered->{$row->{'page'}->{'level'}}->{'object_name'};
          $row->{'page'}->{'icon'} = GetImage(file_id=>$row->{'page'}->{'picture'});
          $row->{'page'}->{'href'} = ConstructURL(node_id=>$row->{'page'}->{'object_id'});
          $row->{'date'} = Engine::Helpers::DateFormater->new(date => $row->{'start_date'});

        push(@items, $row);
      }
    }
=com
  if(my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'),
                            class_name => 'prod_cat')->fetchrow_hashref()){
      my $res = $DB->CAT_MEGAGET(parent_id=>$p->{'object_id'},
                     class_name=>'prod_page',
                     attrs=>['level', 'duration','hours','periodicity','picture','annotation'],
                                   recursive => 1,
                                   on_page => $per_page,
                                   page_num => $page,
                        use_publ_pos=>1);
      my $lc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'level_cat')->fetchrow_hashref;
        my $levels = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level_group', attrs=>['lettered'], use_publ_pos=>1)->fetchall_hashref('object_id');
        my $lettered = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level', attrs=>[], recursive=>1, use_publ_pos=>1)->fetchall_hashref('object_id');
#print "BLA\n";
#print Dumper($levels);
#print Dumper($lettered);
      while(my $row = $res->fetchrow_hashref){
          $row->{'lettered'} = $levels->{$lettered->{$row->{'level'}}->{'parent_id'}}->{'lettered'};
        $row->{'picture'} = GetImage(file_id=>$row->{'picture'});
      $row->{'href'} = ConstructURL(node_id=>$row->{'object_id'});
        push(@items, $row);
      }
    }
=cut
  return {
      items => \@items
    };
}

=com
  Получение дополнительных данных
=cut
sub _getAdvancedInfo {
  my $self = shift;
  my (%attrs) = @_;
  my @items = ();
    my $o = {};
  if(my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'),
                            class_name => 'advantage_cat')->fetchrow_hashref()){
      my $res = $DB->CAT_MEGAGET(parent_id=>$p->{'object_id'},
                     class_name=>'advantage',
                     attrs=>['content'],
                     use_publ_pos=>1);
      while(my $row = $res->fetchrow_hashref){
        push(@items, $row);
      }
        $o = {object_name => $p->{'object_name'}, items => \@items };
    }
  return $o;
}


1;
__END__