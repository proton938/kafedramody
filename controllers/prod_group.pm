package kafedramody::controllers::prod_group;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>[])->fetchrow_hashref()){
		$obj->{'object_name'} = TextCut($obj->{'object_name'}, { removehtml => 1 });
		$data->{'object'} = $obj;
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
	}
    $data->{'courses'} = $self->_getCourses();
    #$data->{'filter'} = $self->_getFilter();
    #print Dumper($data);
}

=com
	Получение курсов направления по группам
=cut
sub _getCourses {
	my $self = shift;
	my @groups = ();

	if(my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'),
												    class_name => 'type_of_educational_program_cat')->fetchrow_hashref()){

    	my $groups = $DB->CAT_MEGAGET(parent_id=>$p->{'object_id'},
    							   class_name=>'type_of_educational_program',
                                   attrs=>[],
                                   use_publ_pos => 1);
        print Dumper($groups);
    	while(my $row = $groups->fetchrow_hashref){
            my $condition = qq{publication};
            $condition .= qq{ AND type_of_educational_program = $row->{'object_id'}};
            print "CONDITION: ".$condition."\n";
        	my @items = ();
        	my $pages = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'),
        							   class_name=>'prod_page',
        							   attrs=>['level','gathering','hours','picture','annotation','position','publication','type_of_educational_program'],
                                       condition => $condition,
                                       recursive => 1,
           							   order_by => [['position','ASC']]);

        	my $lc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'level_cat')->fetchrow_hashref;
            my $levels = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level_group', attrs=>['lettered'], use_publ_pos=>1)->fetchall_hashref('object_id');
            my $lettered = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level', attrs=>['lettered'], recursive=>1, use_publ_pos=>1)->fetchall_hashref('object_id');
            my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'), class_name => 'schedule')->fetchrow_hashref();

        	while(my $page = $pages->fetchrow_hashref){
                my $schedule;
                if ($page->{'gathering'}>0) {
                	$schedule = $DB->CAT_MEGAGET(parent_id=>$p->{'object_id'},
                            class_name=>'schedule_course',
                            attrs=>['subject','start_date','publication'],
                            condition => qq{publication AND start_date > NOW() AND subject IN(:object_id)},
                            limit_rows => 1,
                            recursive => 1,
                            order_by => [['start_date','ASC']],
                            object_id => $page->{'object_id'})->fetchrow_hashref;
            		$schedule->{'start_date'} = Engine::Helpers::DateFormater->new(date => $schedule->{'start_date'});
                }

    			$page->{'href'} = ConstructURL(node_id=>$page->{'object_id'});

        	    $page->{'lettered'} = $levels->{$lettered->{$page->{'level'}}->{'parent_id'}}->{'lettered'};
        	    $page->{'lettered_l'} = $lettered->{$page->{'level'}}->{'lettered'};
        		$page->{'icon'} = GetImage(file_id=>$page->{'picture'});

                push (@items, {
                    page => $page,
                    date => $schedule->{'start_date'},
                    #total_price => $page->{'total_price'},
                    all_hours => $page->{'hours'},
                    #addition => $page->{'periodicity'}
                });
            }
            if (@items > 0) {
                $row->{'items'} = \@items;
                push(@groups, $row);
            }
        }
    }

	return {
	    groups => \@groups,
        schedule_message => $App->Settings->GetParamValue('Messages.NoSchedule'),
    };
}

1;
__END__
