package kafedramody::controllers::__page_404;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $self = $class->SUPER::new(@_);
    bless($self, $class);

    return $self;
}

1;
__END__
