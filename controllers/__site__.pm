package kafedramody::controllers::__site__;

use utf8;
use strict;

use base qw(Engine::Controller);

use Engine::GlobalVars;
use Engine::SysUtils;
use Engine::Helpers;
use Engine::Xslate::Functions qw(XslateFunctions);

use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params) = @_;

    $self->InitTemplateFunctions();

	my $data = {_meta => Metatags('html5'),
				_current_node => $App->Data->Var('current_node'),
                _mobile => $App->Param('__mobile_version'),
				_interface_name => $App->Data->Var('current_node')->{'class_name'} };
	#   -----------------------------------------------------------------

	#   MAIN MENU
	$data->{'main_menu'} = $self->_getMainMenu();
	#   -----------------------------------------------------------------

	#   ADV MENU
	$data->{'adv_menu'} = $self->_getAdvMenu();
	#   -----------------------------------------------------------------

	#   LEVELS MENU
	$data->{'levels_menu'} = $self->_getLevelsMenu();
	#   -----------------------------------------------------------------

	#   PROD MENU
	$data->{'prod_menu'} = $self->_getProdMenu();
	#   -----------------------------------------------------------------

	#   BREADCRUMBS
	if(!$params{'no_breadcrumbs'}){
		$data->{'breadcrumbs'} = $self->_getBreadcrumbs();
	}
	#   -----------------------------------------------------------------

	#   MAIN INFO
	$data->{'site_info'} = $self->_getMainInfo();
	#   -----------------------------------------------------------------

	#   MAIL FORM
	$data->{'form'} = $self->_getMailForm();
	#   -----------------------------------------------------------------

	#   BANNERS
	$data->{'banners'} = { 
        place_1 => $self->_getBanners(1),
        place_2 => $self->_getBanners(2),
        place_3 => $self->_getBanners(3),
        place_4 => $self->_getBanners(4, 1),
        place_5 => $self->_getBanners(5),
    };

    # Проверяем, включен ли dev режим
    my $dev = $App->Param('dev');
    $data->{'dev'} = $dev ? 1 : 0;

    # Проверяем, включен ли old режим
    my $old = $App->Param('old');
    $data->{'old'} = $old ? 1 : 0;

    my $new = $App->Param('new');
    $data->{'new'} = $new ? 1 : 0;

#   -----------------------------------------------------------------
    #print Dumper($data);
	$self->Data($data);

}

#   --------------- PUBLIC -----------------------------------------

#   --------------- PRIVATE -----------------------------------------

sub _getBanners {
    my $self = shift;
    my ($place, $with_amount) = @_;

    my $items = GetBanners($place);
    if ($with_amount) {
        foreach my $item (@$items) {
            $item->Banner->{'amount'} = $DB->CAT_GET_OBJECT_ATTR(object_id => $item->Banner->{'object_id'}, attr_name => 'amount');
        }
    }
    return $items;
}

=com
	Получение данных для главного меню
=cut
sub _getMainMenu {
	my $self = shift;
	my $data = $self->Data();
	my @menu = ();
    my $prev_href = '/';
    my $next_href;
    my $prev_id;
    my $next_id;

	my $res = $DB->GET_MENU_ITEMS(menu_place => 1);
    my $prev_node;
    my $next_flag = 0;
    my $obj = $DB->CAT_GET_OBJECT(object_id=>$App->Param('node_id'))->fetchrow_hashref;
	while(my $row = $res->fetchrow_hashref){
		$row->{'href'} = ConstructURL(node_id=>$row->{'node'});
        if (!$next_href && $obj->{'class_name'} eq 'home_page') {
            $next_href = $row->{'href'};
            $next_id = $row->{'node'};
        }
		$row->{'class'} = (($row->{'node'} == $App->Param('node_id')) || ($row->{'node'} == $App->Param('current'))) ? 'm--active' : '';
        if ($next_flag) {
            $next_href = ConstructURL(node_id=>$row->{'node'});
            $next_id = $row->{'node'};
            $next_flag = 0;
        }
        if (($row->{'node'} == $App->Param('node_id')) || ($row->{'node'} == $App->Param('current'))) {
            $prev_href = ($prev_node) ? ConstructURL(node_id=>$prev_node) : '';
            $prev_id = $prev_node;
            $next_flag = 1;
        }
		#my $sub_res = $DB->CAT_MEGAGET(parent_id=>$row->{'object_id'}, class_name=>'menu_item', attrs=>['node'], use_publ_pos=>1);
		my $sub_res = $DB->CAT_GET_OBJECT_CHILDS_BY_PUBL_POS(parent_id=>$row->{'node'});
		my @sub_menu = ();
		while(my $row_sub = $sub_res->fetchrow_hashref){
			#$row_sub->{'href'} = ConstructURL(node_id=>$row_sub->{'node'});
			$row_sub->{'href'} = ConstructURL(node_id=>$row_sub->{'object_id'});
			$row_sub->{'class'} = ($row_sub->{'object_id'} == $App->Param('node_id')) ? 'm--active' : '';
			if($row_sub->{'class'} eq 'm--active'){
				$row->{'class'} = 'm--active';
			}
			push(@sub_menu, $row_sub);
		}
		$row->{'sub_menu'} = \@sub_menu;
		push(@menu, $row);
        $prev_node = $row->{'node'};
	}

	return {
	    items => \@menu,
        prev_href => $prev_href,
        next_href => $next_href,
        prev_id => $prev_id,
        next_id => $next_id
    };
}

=com
	Получение данных для меню уровней
=cut
sub _getLevelsMenu {
	my $self = shift;
	my $data = $self->Data();
	my @menu = ();

	if (my $lc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'level_cat')->fetchrow_hashref) {
        my $letters = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level_group', attrs=>['lettered'], use_publ_pos=>1);
        my $pc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('p_c'), class_name=>'prod_cat')->fetchrow_hashref;
        my $pg = $DB->CAT_MEGAGET(parent_id=>$pc->{'object_id'}, class_name=>'prod_group', attrs=>[], use_publ_pos=>1)->fetchall_arrayref;
		while(my $letter = $letters->fetchrow_hashref){
        	my @groups = ();
    		foreach my $group (@{$pg}){
                my $subletters = $DB->CAT_MEGAGET(parent_id=>$letter->{'object_id'}, class_name=>'level', attrs=>['lettered'], use_publ_pos=>1);
            	my @sublevels = ();
                my $count = 0;
        		while(my $subletter = $subletters->fetchrow_hashref){
                    my $pp = $DB->CAT_MEGAGET(parent_id=>@$group[0], class_name=>'prod_page', attrs=>['level','publication','position'], condition=>qq{publication AND level=$subletter->{'object_id'}});
                	my @pages = ();
            		while(my $page = $pp->fetchrow_hashref){
            			$page->{'href'} = ConstructURL(node_id=>$page->{'object_id'});
                		push(@pages, $page);
            		}
                    $count = $count + @pages;
                    $subletter->{'pages'} = \@pages;
            		push(@sublevels, $subletter);
                }
                my $g = {
                    object_id => @$group[0],
                    object_name => TextCut(@$group[3], { removehtml => 1 }),
        			href => ConstructURL(node_id=>@$group[0]),
                    sublevels => \@sublevels,
                    count => $count
                };
        		push(@groups, $g);
            }
            $letter->{'groups'} = \@groups;
    		push(@menu, $letter);
        }
	}
    #print Dumper(@menu);
	return \@menu;
}

=com
	Получение данных для дополнительного меню
=cut
sub _getAdvMenu {
	my $self = shift;
	my $data = $self->Data();
	my $sch;
    my $form;

	if ($sch = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'), class_name=>'schedule')->fetchrow_hashref) {
        $sch->{'href'} = ConstructURL(node_id=>$sch->{'object_id'});
        $sch->{'class'} = ($sch->{'object_id'} == $App->Param('node_id')) ? 'm--active' : '';
	}
	if ($form = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'), class_name=>'page_with_form', attrs=>['_form_class_name'], condition=>qq{_form_class_name = 'form_course'})->fetchrow_hashref) {
        $form->{'href'} = ConstructURL(node_id=>$form->{'object_id'});
        $form->{'class'} = ($form->{'object_id'} == $App->Param('node_id')) ? 'm--active' : '';
	}
    #print Dumper(@menu);
	return {
	    schedule => $sch,
        form => $form
	};
}

=com
	Получение данных для меню направлений курсов
=cut
sub _getProdMenu {
	my $self = shift;
	my $data = $self->Data();
	my @menu = ();

	if (my $pc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'), class_name=>'prod_cat')->fetchrow_hashref) {
        my $groups = $DB->CAT_MEGAGET(parent_id=>$pc->{'object_id'}, class_name=>'prod_group', attrs=>['icon'], use_publ_pos=>1);

		while(my $group = $groups->fetchrow_hashref){
      		$group->{'icon'} = GetImage(file_id=>$group->{'icon'});
			$group->{'href'} = ConstructURL(node_id=>$group->{'object_id'});
			$group->{'class'} = ($group->{'object_id'} == $App->Param('node_id') || $group->{'object_id'} == $App->Param('subcurrent')) ? 'm--active' : '';
    		push(@menu, $group);
        }
	}
    #print Dumper(@menu);
	return {
	    items => \@menu
    };
}

=com
	Получение данных для фильтра
=cut
sub _getFilter {
	my $self = shift;
	my $data = $self->Data();

	my @filter_letters = split(',', $App->Session->Var('_FILTER_LETTERS'));
    if ($App->Param('letters')) {
        $App->Session->Var('_FILTER_LETTERS', $App->Param('letters'));
        @filter_letters = split(',', $App->Param('letters'));
    }
	my @filter_times = split(',', $App->Session->Var('_FILTER_TIMES'));
    if ($App->Param('times')) {
        $App->Session->Var('_FILTER_TIMES', $App->Param('times'));
        @filter_times = split(',', $App->Param('times'));
    }
	my @filter_addresses = split(',', $App->Session->Var('_FILTER_ADDRESSES'));
    if ($App->Param('addresses')) {
        $App->Session->Var('_FILTER_ADDRESSES', $App->Param('addresses'));
        @filter_addresses = split(',', $App->Param('addresses'));
    }

	my @letters = ();
	if (my $lc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'level_cat')->fetchrow_hashref) {
        my $lg = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level_group', attrs=>['lettered'], use_publ_pos=>1);
		while(my $group = $lg->fetchrow_hashref){
		    my @levels = ();
            my $l = $DB->CAT_MEGAGET(parent_id=>$group->{'object_id'}, class_name=>'level', attrs=>['lettered'], use_publ_pos=>1);
    		while(my $level = $l->fetchrow_hashref){
    		    $group->{'class'} = (grep{$_ == $level->{'object_id'}}@filter_letters) ? 'm--active' : '';
    		    push (@levels, $level->{'object_id'});
    		}
            $group->{'childs_id'} = join(',', @levels);
            push (@letters, $group);
        }
    }

	my @times = ();
	if (my $tc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'times_of_day_cat')->fetchrow_hashref) {
        my $t = $DB->CAT_MEGAGET(parent_id=>$tc->{'object_id'}, class_name=>'times_of_day', attrs=>[], use_publ_pos=>1);

		while(my $time = $t->fetchrow_hashref){
		    $time->{'class'} = (grep{$_ == $time->{'object_id'}}@filter_times) ? 'm--active' : '';
    		push(@times, $time);
        }
	}

	my @addresses = ();
	if (my $ac = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'adress_cat')->fetchrow_hashref) {
        my $a = $DB->CAT_MEGAGET(parent_id=>$ac->{'object_id'}, class_name=>'adress', attrs=>[]);

		while(my $address = $a->fetchrow_hashref){
		    $address->{'class'} = (grep{$_ == $address->{'object_id'}}@filter_addresses) ? 'm--active' : '';
    		push(@addresses, $address);
        }
	}

	return {
	    addresses => \@addresses,
	    letters => \@letters,
	    times => \@times
    };
}

# Получение хлебных крошек
sub _getBreadcrumbs {
	my $self = shift;
	my $node_id = shift;

	$node_id = $App->Param('node_id') unless (defined($node_id));

	my @breadcrumbs = ();

	my $res = $DB->CAT_GET_PATH_TO_NODE(from_id=>$App->Param('current'),
										node_id=>$node_id,
										result_type=>'sql');
	push(@breadcrumbs, { href=> '/',
                         _active=>0,
						 object_name=>'Главная' });
	while(my $row = $res->fetchrow_hashref){
		$row->{'_active'} = ($row->{'object_id'} == $node_id)?1:0;
		$row->{'href'} = ConstructURL(node_id=>$row->{'object_id'});
		$row->{'object_name'} = TextCut($row->{'object_name'}, { removehtml => 1 });
		push(@breadcrumbs, $row);
	}

	return \@breadcrumbs;
}

=com
	Получение общих данных
=cut
sub _getMainInfo {
	my $self = shift;

    my $social_attrs = $DB->CAT_GET_CLASS_ATTRS(class_name => 'site_info', attr_group_name => 'social')->fetchall_arrayref({});
	if (my $obj = $DB->CAT_MEGAGET(parent_id=>$App->Param('a_c'), class_name=>'site_info', attrs=>['site_name', 'contacts', 'map', 'footer_1', 'footer_2', map { $_->{'attr_name'} } @$social_attrs])->fetchrow_hashref()) {
        my @socials = ();
        for (my $i = 0; $i < @$social_attrs; $i ++) {
            my $attr = $social_attrs->[$i];
            if ($obj->{ $attr->{'attr_name'} }) {
                $attr->{'value'} = $obj->{ $attr->{'attr_name'} };
                push @socials, $attr;
            }
        }
        $obj->{'socials'} = \@socials;

        return $obj;
    }
    return undef;
}

=com
	Получение Формы обратной связи для футера
=cut
sub _getMailForm {
	my $self = shift;
	my $form = undef;

	if(my $obj = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'),
                                class_name => 'page_with_form',
	                            attrs => ['_form_class_name','_content','_s_content'],
                                   condition=>qq{_form_class_name = 'form_mail'})->fetchrow_hashref()){
		$form->{'object'} = $obj;
		if(($App->Session->Var('FORMS_' . $obj->{'_form_class_name'}) eq 'SENT') and (!$App->Param('form_id'))){
			$form->{'object'}->{'_sent'} = 1;
			$App->Session->UndefVar('FORMS_' . $obj->{'_form_class_name'});
		}else{
			if($obj->{'_form_class_name'}){
				$form = $self->ConstructForm($obj);
			}
		}
        $form->{'object'} = $obj;
	}
	return $form;
}

sub ConstructForm{
	my $self = shift;
	my $obj = shift;

	# Функция преобработки поля формы
	my $preprocess = sub() {
		my $input = shift; # input
		my $field = shift; # field описание поля, для которого делаем input

		# print Dumper($input);
		# print Dumper($field);

		# Сюда передается одиночное поле. Не массив. Перебирать что-то через foreach бесмысленно
		# Достаточно написать просто
		$input->{'class'} = 'inlineBox_item form__form-control';
		if ($obj->{'_form_class_name'} eq 'form_req') {
			$input->{'placeholder'} = $field->{'publ_comment'};
		}
		return $input;
	};

	my $form = {
				action  => ConstructURL( node_id => $obj->{'object_id'} ),
				form_id => $obj->{'object_id'},
				_action => 'send_form',
				_form_class_name => $obj->{'_form_class_name'},
				fields  => GetForm(form_class  => $obj->{'_form_class_name'},
								   attrs_group => 'display', class => 'form-control', preprocess => $preprocess),
				submit_text => $App->Settings->GetParamValue('Forms.SubmitLabel'),
				remark_text => $App->Settings->GetParamValue('Forms.RemarkText'),

			};
	if (!$App->Data->Var('__AJAX_REQUEST')) {
		$form->{'captcha'} = {
								src => CreateCaptcha(color1 => "#993a97",
													 color2 => "#cccccc",
													 type => 2),
								class => ($App->Data->Var('__CAPTCHA_ERROR')) ? 'captcha has-error' : 'captcha',
								captcha_text => $App->Settings->GetParamValue('Forms.CaptchaLabel')
							};
	}
	return $form;
}

sub InitTemplateFunctions {
    my $self = shift;
    my $functions = XslateFunctions();

    $self->Templates(
        function => $functions
    );
}

1;
__END__
