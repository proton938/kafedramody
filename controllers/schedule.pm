package kafedramody::controllers::schedule;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>[])->fetchrow_hashref()){
		$data->{'object'} = $obj;
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
	}
    $data->{'schedules'} = $self->_getSchedules();
    $data->{'filter'} = $self->_getFilter();
    if ($App->Param('__mobile_version')==1) {
  		$data->{'show_type'} = 'list';
    } else {
    	if (defined($App->Session->Var('_SHOW_TYPE'))) {
    		$data->{'show_type'} = $App->Session->Var('_SHOW_TYPE');
    	} else {
    		$data->{'show_type'} = 'table';
	    }
  	}
    #print Dumper($data);
}

=com
	Получение курсов
=cut
sub _getSchedules {
	my $self = shift;
	my (%attrs) = @_;
	my @items = ();
    my $total;

    $App->Param('page', 1) unless (defined $App->Param('page'));
    my $page = $App->Param('page');
    my $per_page = $App->Settings->GetParamValue('XCatalog.SchedulePerPage') || 10;
    my @order_by;
    $App->Session->Var('_SHOW_TYPE', 'table') if (!defined($App->Session->Var('_SHOW_TYPE')));
    if (defined($App->Session->Var('_SHOW_TYPE')) && $App->Session->Var('_SHOW_TYPE') eq 'table') {
        $page = 1;
        $per_page = 999999;
        @order_by = [['adress','ASC'],['start_date','ASC']];
    } else {
        @order_by = [['start_date','ASC']];
    }

    my @fields = qw/letter/;
    my $values = { map { ($App->Param($_) ne '') ? ($_ => $App->Param($_)) : () } @fields };

	if(my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'),
												    class_name => 'prod_cat')->fetchrow_hashref()){

        my $condition = qq{publication};
        $condition .= qq{ AND level IN(}.$App->Session->Var('_FILTER_LETTERS').qq{)} if ($App->Session->Var('_FILTER_LETTERS'));
        print "CONDITION: ".$condition."\n";
    	my $pages = $DB->CAT_MEGAGET(parent_id=>$p->{'object_id'},
    							   class_name=>'prod_page',
    							   attrs=>['level','hours','picture','annotation','position','publication'],
                                   condition => $condition,
                                   recursive => 1,
       							   order_by => [['position','ASC']])->fetchall_hashref('object_id');
        my @pages_array;
        foreach my $key (keys $pages) {
            push @pages_array, $key;
        }
        my $pages_list = join(',', @pages_array);
        my $condition = qq{publication AND start_date >= NOW() AND subject IN($pages_list)};
        $condition .= qq{ AND times_of_day IN(}.$App->Session->Var('_FILTER_TIMES').qq{)} if ($App->Session->Var('_FILTER_TIMES'));
        $condition .= qq{ AND adress IN(}.$App->Session->Var('_FILTER_ADDRESSES').qq{)} if ($App->Session->Var('_FILTER_ADDRESSES'));
        print "CONDITION: ".$condition."\n";
    	my $schedules = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'),
    							   class_name=>'schedule_course',
    							   attrs=>['subject','adress','all_hours','times_of_day','start_date','end_date','time_of_training','addition','total_price','discount','publication','recruitment'],
                                   group_by => ['nodes.id'],
                                   condition => $condition,
                                   on_page => $per_page,
                                   page_num => $page,
       							   recursive => 1,
                                   order_by => @order_by);

    	my $res = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'),
    							   class_name=>'schedule_course',
    							   attrs=>['subject','all_hours','times_of_day','start_date','time_of_training','addition','total_price','publication','recruitment'],
                                   group_by => ['nodes.id'],
                                   condition => $condition,
       							   recursive => 1)->fetchall_arrayref({});
        $total = @$res;
        print "Total: ".$total."\n";
    	my $lc = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'level_cat')->fetchrow_hashref;
        my $levels = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level_group', attrs=>['lettered'], use_publ_pos=>1)->fetchall_hashref('object_id');
        my $lettered = $DB->CAT_MEGAGET(parent_id=>$lc->{'object_id'}, class_name=>'level', attrs=>['lettered'], recursive=>1, use_publ_pos=>1)->fetchall_hashref('object_id');
    	my $ac = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('a_c'), class_name=>'adress_cat')->fetchrow_hashref;
        my $addresses = $DB->CAT_MEGAGET(parent_id=>$ac->{'object_id'}, class_name=>'adress', attrs=>[])->fetchall_hashref('object_id');

        my $date_devider = '00';
        my $addr_devider = 0;
    	while(my $row = $schedules->fetchrow_hashref){
    	    $row->{'page'} = $pages->{$row->{'subject'}};
    	    $row->{'page'}->{'lettered'} = $levels->{$lettered->{$row->{'page'}->{'level'}}->{'parent_id'}}->{'lettered'};
    	    $row->{'page'}->{'lettered_l'} = $lettered->{$row->{'page'}->{'level'}}->{'lettered'};
    		$row->{'page'}->{'icon'} = GetImage(file_id=>$row->{'page'}->{'picture'});
			$row->{'page'}->{'href'} = ConstructURL(node_id=>$row->{'page'}->{'object_id'});
    		$row->{'addr_string'} = $addresses->{$row->{'adress'}}->{'object_name'};
    		$row->{'date'} = Engine::Helpers::DateFormater->new(date => $row->{'start_date'});
    		$row->{'date_end'} = Engine::Helpers::DateFormater->new(date => $row->{'end_date'});

            if ($date_devider ne $row->{'date'}->html('%m')) {
                $date_devider = $row->{'date'}->html('%m');
                $row->{'date_devider'} = 1;
            }

            if ($addr_devider != $row->{'adress'}) {
                $addr_devider = $row->{'adress'};
                $row->{'addr_devider'} = 1;
            }

            my @teacher = $DB->CAT_GET_OBJECT_ATTR(object_id=>$row->{'object_id'}, attr_name=>'teacher', result_type=>'value');
            my @array;
            foreach (@teacher) {
                my $t = $DB->CAT_GET_OBJECT(object_id=>$_)->fetchrow_hashref;
    			$t->{'href'} = ConstructURL(node_id=>$t->{'object_id'});
                push (@array, $t);
            }
            $row->{'teacher'} = \@array;
            my $day = $DB->CAT_GET_OBJECT(object_id=>$row->{'times_of_day'})->fetchrow_hashref;
            my $address = $DB->CAT_GET_OBJECT(object_id=>$row->{'adress'})->fetchrow_hashref;
            $row->{'times_of_day'} = $day->{'object_name'};
            $row->{'address'} = $address->{'object_name'};
            my $form_page = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'), class_name=>'page_with_form', attrs=>['_form_class_name'], condition=>qq{_form_class_name='form_course'})->fetchrow_hashref;
            $form_page->{'href'} = ConstructURL(node_id=>$form_page->{'object_id'}, schedule=>$row->{'object_id'});
            $row->{'form_page'} = $form_page;

    		push(@items, $row);
    	}
    }
	return {
	    items => \@items,
        schedule_message => $App->Settings->GetParamValue('Messages.NoSchedule'),
        navbar => NavBar(
            count => $total,
            page => $page,
            per_page => $per_page,
            no_first => 0,
            url_params => { node_id => $App->Param('node_id'), %$values }
        )
    };
}

1;
__END__
