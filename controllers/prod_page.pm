package kafedramody::controllers::prod_page;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>['level','hours','form_text','type_of_educational_program','picture','annotation','content','block_1','block_2','block_3','results','result_block_1','result_block_2','result_block_3','programm','image_1','image_2','image_3','image_4','show_level'])->fetchrow_hashref()){
        my $level = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$obj->{'level'}, attrs=>['lettered'])->fetchrow_hashref();
        my $level_group = $DB->CAT_GET_OBJECT(object_id=>$level->{'parent_id'})->fetchrow_hashref();
        $level->{'parent_name'} = $level_group->{'object_name'};
        $obj->{'level'} = $level;
		$obj->{'image_1'} = GetImage(file_id=>$obj->{'image_1'});
		$obj->{'image_2'} = GetImage(file_id=>$obj->{'image_2'});
		$obj->{'image_3'} = GetImage(file_id=>$obj->{'image_3'});
		$obj->{'image_4'} = GetImage(file_id=>$obj->{'image_4'});
        $obj->{'form'} = $self->getForm();
		$data->{'object'} = $obj;
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
	}

    $data->{'schedules'} = $self->_getSchedules();
    $data->{'reviews'} = $self->_getReviews();
    #print Dumper($data);
}

=com
	Получение списка расписаний
=cut
sub _getSchedules {
	my $self = shift;
    my @schedule = ();

    my $p = $DB->CAT_GET_OBJECT_BY_CLASS_NAME(parent_id=>$App->Param('i_c'),
				                class_name => 'schedule')->fetchrow_hashref();
	my $schedules = $DB->CAT_MEGAGET(parent_id=>$p->{'object_id'},
							   class_name=>'schedule_course',
							   attrs=>['subject','adress','all_hours','times_of_day','start_date','time_of_training','addition','total_price','discount','publication','recruitment'],
                               condition => qq{publication AND start_date >= NOW() AND subject IN(:object_id)},
                               limit_rows => 3,
   							   recursive => 1,
                               order_by => [['start_date','ASC']],
                               object_id => $App->Param('node_id'));

	while(my $row = $schedules->fetchrow_hashref){

        my @teacher = $DB->CAT_GET_OBJECT_ATTR(object_id=>$row->{'object_id'}, attr_name=>'teacher', result_type=>'value');
        my @array;
        foreach (@teacher) {
            my $t = $DB->CAT_GET_OBJECT(object_id=>$_)->fetchrow_hashref;
			$t->{'href'} = ConstructURL(node_id=>$t->{'object_id'});
            push (@array, $t);
        }
        $row->{'teacher'} = \@array;
		$row->{'date'} = Engine::Helpers::DateFormater->new(date => $row->{'start_date'});
        my $day = $DB->CAT_GET_OBJECT(object_id=>$row->{'times_of_day'})->fetchrow_hashref;
        my $address = $DB->CAT_GET_OBJECT(object_id=>$row->{'adress'})->fetchrow_hashref;
        $row->{'times_of_day'} = $day->{'object_name'};
        $row->{'address'} = $address->{'object_name'};
        my $form_page = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'), class_name=>'page_with_form', attrs=>['_form_class_name'], condition=>qq{_form_class_name='form_course'})->fetchrow_hashref;
        $form_page->{'href'} = ConstructURL(node_id=>$form_page->{'object_id'}, schedule=>$row->{'object_id'});
        $row->{'form_page'} = $form_page;
		push(@schedule, $row);
	}

    return {
        items => \@schedule
    }

}

=com
	Получение списка отзывов
=cut
sub _getReviews {
	my $self = shift;
    my @review = ();

	my $reviews = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'),
							   class_name=>'review',
							   attrs=>['addition_date','person','review','publication'],
                               condition => qq{publication},
                               order_by => [['addition_date','DESC']]);
	while(my $row = $reviews->fetchrow_hashref){
		$row->{'date'} = Engine::Helpers::DateFormater->new(date => $row->{'addition_date'});
		push(@review, $row);
    }
    my $form_page = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'), class_name=>'page_with_form', attrs=>['_form_class_name'], condition=>qq{_form_class_name='review'})->fetchrow_hashref;
    $form_page->{'href'} = ConstructURL(node_id=>$form_page->{'object_id'}, course=>$App->Param('node_id'));
    return {
        items => \@review,
        form_page => $form_page,
        reviews_message => $App->Settings->GetParamValue('Messages.NoReview')
    }

}

=com
    Получение Формы для заявки
=cut
sub getForm {
    my $self = shift;
    my $form = undef;

    if(my $obj = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'),
                                class_name => 'page_with_form',
                                attrs => ['_form_class_name','_content','_s_content'],
                                   condition=>qq{_form_class_name = 'form_req'})->fetchrow_hashref()){
        $form->{'object'} = $obj;
        if(($App->Session->Var('FORMS_' . $obj->{'_form_class_name'}) eq 'SENT') and (!$App->Param('form_id'))){
            $form->{'object'}->{'_sent'} = 1;
            $App->Session->UndefVar('FORMS_' . $obj->{'_form_class_name'});
        }else{
            if($obj->{'_form_class_name'}){
                $form = $self->ConstructForm($obj);
            }
        }
        $form->{'object'} = $obj;
    }
    return $form;
}

1;
__END__
