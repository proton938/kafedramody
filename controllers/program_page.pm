package kafedramody::controllers::program_page;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'),
												attrs=>['type_of_educational_program','picture','annotation','content','block_1','block_2','block_3','results','result_block_1','result_block_2','result_block_3'])->fetchrow_hashref()){
        my $level = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$obj->{'level'}, attrs=>['lettered'])->fetchrow_hashref();
        my $level_group = $DB->CAT_GET_OBJECT(object_id=>$level->{'parent_id'})->fetchrow_hashref();
        $level->{'parent_name'} = $level_group->{'object_name'};
        $obj->{'level'} = $level;

		$data->{'object'} = $obj;
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
	}

    $data->{'academic'} = $self->_getAcademicPlan();
    $data->{'reviews'} = $self->_getReviews();
    #print Dumper($data);
}

=com
	Получение списка академических планов
=cut
sub _getAcademicPlan {
	my $self = shift;
    my @academic = ();
    my @res = $DB->CAT_GET_OBJECT_ATTR(object_id=>$App->Param('node_id'), attr_name=>'academic_plan', result_type=>'value');
   	foreach my $row (@res){
        my $a = $DB->CAT_GET_OBJECT(object_id=>$row)->fetchrow_hashref();
        $a->{'href'} = ConstructURL(node_id=>$a->{'object_id'});
        push(@academic, $a);
    }
    return \@academic;
}

=com
	Получение списка отзывов
=cut
sub _getReviews {
	my $self = shift;
    my @review = ();

	my $reviews = $DB->CAT_MEGAGET(parent_id=>$App->Param('node_id'),
							   class_name=>'review',
							   attrs=>['addition_date','person','review','publication'],
                               condition => qq{publication},
                               order_by => [['addition_date','DESC']]);
	while(my $row = $reviews->fetchrow_hashref){
		$row->{'date'} = Engine::Helpers::DateFormater->new(date => $row->{'addition_date'});
		push(@review, $row);
    }
    my $form_page = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'), class_name=>'page_with_form', attrs=>['_form_class_name'], condition=>qq{_form_class_name='review'})->fetchrow_hashref;
    $form_page->{'href'} = ConstructURL(node_id=>$form_page->{'object_id'}, course=>$App->Param('node_id'));
    return {
        items => \@review,
        form_page => $form_page,
        reviews_message => $App->Settings->GetParamValue('Messages.NoReview')
    }

}

1;
__END__
