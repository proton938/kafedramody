package kafedramody::controllers::reviews_page;

use utf8;
use strict;

use base qw(kafedramody::controllers::__site__);

use Engine::GlobalVars;
use Engine::Helpers;
use Engine::Helpers::DateFormater;
use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;

	my $self = $class->SUPER::new(@_);
	bless($self, $class);

	return $self;
}

sub Init {
	my $self = shift;
	my (%params)  = @_;
	$self->SUPER::Init(%params);
	$self->GetObject();
}

sub GetObject{
	my $self = shift;
	my $data = $self->Data();

	if(my $obj = $DB->CAT_GET_OBJECT_WITH_ATTRS(object_id=>$App->Param('node_id'), attrs=>['content'])->fetchrow_hashref()){
		$data->{'object'} = $obj;
    	my @gallery = ();
    	if ( my $res = $DB->CAT_GET_OBJECT_GALLERY(object_id=>$obj->{'object_id'}, publication=>1)) {
        	while(my $row = $res->fetchrow_hashref){
        		push(@gallery, $row);
        	}
            $data->{'gallery'} = \@gallery;
        }
	}
    $data->{'reviews'} = $self->_getReviews();
    #print Dumper($data);
}

=com
	Получение списка отзывов
=cut
sub _getReviews {
	my $self = shift;
    my @review = ();
    my $total;

    $App->Param('page', 1) unless (defined $App->Param('page'));
    my $page = $App->Param('page');
    my $per_page = $App->Settings->GetParamValue('XCatalog.ReviewPerPage') || 10;

    my @fields = qw/course/;
    my $values = { map { ($App->Param($_) ne '') ? ($_ => $App->Param($_)) : () } @fields };

	my $reviews = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'),
                class_name=>'review',
                attrs=>['addition_date','person','review','publication'],
                condition => qq{publication},
                recursive => 1,
                on_page => $per_page,
                page_num => $page,
                order_by => [['addition_date','DESC']]);
        while(my $row = $reviews->fetchrow_hashref){
	    my $course = $DB->CAT_GET_OBJECT(object_id=>$row->{'parent_id'})->fetchrow_hashref;
		$row->{'date'} = Engine::Helpers::DateFormater->new(date => $row->{'addition_date'});
        $row->{'href'} = ConstructURL(node_id=>$course->{'object_id'});
        $row->{'course'} = $course;
		push(@review, $row);
    }
	my $total = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'),
                class_name=>'review',
                attrs=>['addition_date','person','review','publication'],
                condition => qq{publication},
                recursive => 1,
                count_only => 1);
    my $form_page = $DB->CAT_MEGAGET(parent_id=>$App->Param('i_c'), class_name=>'page_with_form', attrs=>['_form_class_name'], condition=>qq{_form_class_name='review'})->fetchrow_hashref;
    $form_page->{'href'} = ConstructURL(node_id=>$form_page->{'object_id'});
    return {
        items => \@review,
        form_page => $form_page,
        reviews_message => $App->Settings->GetParamValue('Messages.NoReview'),
        navbar => NavBar(
            count => $total,
            page => $page,
            per_page => $per_page,
            no_first => 0,
            url_params => { node_id => $App->Param('node_id'), %$values }
        )
    }
}

1;
__END__
