package kafedramody::dblib::Tools;

use strict;
no strict "refs";

use Engine::GlobalVars;
use Engine::SysUtils;

use utf8;

sub import {
    my $from = __PACKAGE__;
    my $to=caller();
    no strict "refs";
    while(my ($k,$v) = each(%{"$from\::"})) {
        next if substr($k,-1) eq ":" || grep { $k eq $_ } qw(BEGIN);
        *{"$to\::$k"}=$v;
    }
}

=com
    Форк получения метатегов.
=cut
sub GET_HEAD_NEW {
    my $self = shift;
    my(%params) = @_;

    my $answer = {};

    my @title;
    my @description;
    my @keywords;

    push @title, $App->Settings->GetParamValue('Seo.SiteName');
    if($App->Param('page')) {
        if(my $object = $self->CAT_GET_OBJECT_WITH_ATTRS(
                object_id => $App->Param('node_id'),
                attrs     => ['title','description','keywords']
            )->fetchrow_hashref()) {
            push @title, $object->{'title'};
            push @keywords, $object->{'keywords'};
            push @description, $object->{'description'};
        }

        push @title, $App->Settings->GetParamValue('Seo.Title');
        push @description, $App->Settings->GetParamValue('Seo.Description');
        push @keywords, $App->Settings->GetParamValue('Seo.Keywords');

        $answer = {
            title       => join(' ', grep { $_ } @title),
            description => join(' ', grep { $_ } @description),
            keywords    => join(' ', grep { $_ } @keywords)
        };
    }else{
        $answer = SQL4Services::Tools::GET_HEAD_NEW($self, @_);
    }

    $answer->{'title'} .= ' — Страница ' . int($App->Param('page')) if (int($App->Param('page')) > 0);
    $answer->{'description'} .= ' Страница ' . int($App->Param('page')) if (int($App->Param('page')) > 0);

    return $answer;
}

1;
